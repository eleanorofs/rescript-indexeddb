# rescript-indexeddb

**Warning: I haven't written tests for this package yet. Use at your own risk,
and feel free to open issues.**

## Implemented
- [X] [IDBCursor](https://developer.mozilla.org/en-US/docs/Web/API/IDBCursor)
- [X] [IDBCursorWithValue](https://developer.mozilla.org/en-US/docs/Web/API/IDBCursorWithValue) (see binding for IDBCursor).
- [X] [IDBCursorDirection](https://developer.mozilla.org/en-US/docs/Web/API/IDBCursor/direction)
- [X] [IDBCursorSource](https://developer.mozilla.org/en-US/docs/Web/API/IDBCursor/source)
- [X] [IDBDatabase](https://developer.mozilla.org/en-US/docs/Web/API/IDBDatabase)
- [X] [IDBFactory](https://developer.mozilla.org/en-US/docs/Web/API/IDBFactory)
- [X] [IDBIndexParameters](https://developer.mozilla.org/en-US/docs/Web/API/IDBObjectStore/createIndex)
- [X] [IDBKeyRange](https://developer.mozilla.org/en-US/docs/Web/API/IDBKeyRange)
- [X] [IDBObjectStore](https://developer.mozilla.org/en-US/docs/Web/API/IDBObjectStore)
- [X] [IDBObjectStoreKeyPath](https://developer.mozilla.org/en-US/docs/Web/API/IDBObjectStore/keyPath)
- [X] [IDBOpenDBRequest](https://developer.mozilla.org/en-US/docs/Web/API/IDBOpenDBRequest)
- [X] [IDBRequest](https://developer.mozilla.org/en-US/docs/Web/API/IDBRequest)
- [X] [IDBRequestReadyState](https://developer.mozilla.org/en-US/docs/Web/API/IDBRequest/readyState)
- [X] [IDBTransaction](https://developer.mozilla.org/en-US/docs/Web/API/IDBTransaction)
- [X] [IDBTransactionDurability](https://developer.mozilla.org/en-US/docs/Web/API/IDBTransaction/durability)
- [X] [IDBTransactionMode](https://developer.mozilla.org/en-US/docs/Web/API/IDBTransaction/mode)
- [X] [IDBVersionChangeEvent](https://developer.mozilla.org/en-US/docs/Web/API/IDBVersionChangeEvent)

## Build
```
npm run build
```

## Test

I can't seem to get Puppeteer to work, and that's not my main priority for now,
but you can manually run an http server and navigate to */test.html* to see 
it working. 

If any Puppeteer experts are reading this, feel free to help educate me!

## Usage

I plan to expand on this later. For now, please refer to 
*/test/browser/Index.res*.

## License
This software is available under two licenses. 

* an adaptation of the Do No Harm license which I've called the No Violence
license. 
* the MIT license.

Both are available in this directory. 

