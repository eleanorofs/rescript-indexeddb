type t;

module Page = {
  type t;

  @send external
  evaluate: (t, () => 'serializable) => option<'serializable> = "evaluate";

  @send external
  goto: (t, string) => Promise.t<unit> = "goto";

  
};

module Browser = {
  type t;

  @send external newPage: t => Promise.t<Page.t> = "newPage";
};

@module("puppeteer") external puppeteer: t = "default";

@send external launch: t => Promise.t<Browser.t> = "launch";

