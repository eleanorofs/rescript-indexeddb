let evaluate = (page: Puppeteer.Page.t, script: () => 't):
    Promise.t<<option<'t>> => 
    page -> Puppeteer.Page.goto("localhost:3000/test.html")
    -> Promise.thenResolve(() => page -> Puppeteer.Page.evaluate(script))
  ;
                    

let _ = Puppeteer.puppeteer -> Puppeteer.launch
    -> Promise.then(browser => browser -> Puppeteer.Browser.newPage)
    -> Promise.then(page => page -> evaluate(() => Index.asyncDBPromise()))
    -> Promise.thenResolve(_ => Vigil.Assert.condition({
      message: "Database creation should be successful.",
      operator: "database and collection creation.",
      fn: () => true
    }))
    -> Promise.catch(ex => {
      Js.Console.error(ex);
      Vigil.Assert.condition({
      message: "Database creation should be successful.",
      operator: "database and collection creation.",
      fn: () => false
      }) -> Promise.resolve
    })
    -> Promise.thenResolve(a => Vigil.Test.runSuite([ a ],
                                             "database creation",
                                             Vigil.Configuration.default
                                            )
                   );

  
