@val external window: Dom.window = "window";

let setUpDB = (): IDBRequest.t<IDBDatabase.t> => {
  let openRequest = window -> Window.indexedDB
      -> IDBFactory.Open.withVersion("tododb", 1.);

  openRequest -> IDBOpenDBRequest.set_onupgradeneeded(versionChangeEvent => {
    let db = versionChangeEvent
        -> IDBVersionChangeEvent.target
        -> IDBRequest.result;
    let todos = db
        -> IDBDatabase.CreateObjectStore.withOptions("todos", {
          keyPath: String("id"),
          autoIncrement: false
        });
    let _ = todos
        -> IDBObjectStore.CreateIndex.withParameters("name", #String("name"), {
          unique: false,
          multiEntry: false,
          locale: None
        }); 
  });
  
  openRequest;
};

let asyncDBPromise = (): Promise.t<IDBDatabase.t> =>
    Promise.make((resolve, reject) => {
      let dbReq = setUpDB();
      dbReq -> IDBRequest.set_onsuccess(_ => {
        resolve(. dbReq -> IDBRequest.result);
      });
      dbReq -> IDBRequest.set_onerror(_ => {
        reject(. dbReq -> IDBRequest.error);
      });
    });

let _ = asyncDBPromise()
  -> Promise.thenResolve(db => Js.Console.log(db))
    -> Promise.catch(ex => {
      Js.Console.error(ex);
      Promise.reject(ex);
    });
