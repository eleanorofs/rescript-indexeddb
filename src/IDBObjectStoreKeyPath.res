type t = Null | String(string) | Array(Js.Array.t<string>);

type jsShared;

let toJsShared = (t: t): jsShared => {
  switch(t) {
  | Null => Obj.magic(());
  | String(str) => Obj.magic(str);
  | Array(arr) => Obj.magic(arr);
  };
};


    

