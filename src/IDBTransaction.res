type t = Types.iDBTransaction;

/* properties */

@get external db: t => Types.iDbDatabase = "db";

@get external durability: t => IDBTransactionDurability.t = "durability";

@get external error: t => Js.Nullable.t<Js.Exn.t> = "error";

@get external mode: t => IDBTransactionMode.t = "mode";

@get
  external objectStoreNames: t => Js.Array.t<string> = "objectStoreNames";

/* methods */

@send external abort: t => unit = "abort";

@send external objectStore: (t, string) => IDBObjectStore.t = "objectStore";

@send external commit: t => unit = "commit";

/* event listeners */

type eventHandler = Dom.event => unit;

@get external get_onabort: t => eventHandler = "onabort";

@set external set_onabort: (t, eventHandler) => unit = "onabort";

@get external get_oncomplete: t => eventHandler = "oncomplete";

@set external set_oncomplete: (t, eventHandler) => unit = "oncomplete";

@get external get_onerror: t => eventHandler = "onerror";

@set external set_onerror: (t, eventHandler) => unit = "onerror";

