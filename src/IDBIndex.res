type t = Types.iDbIndex;

/* Properties */

@get external isAutoLocale: t => option<bool> = "isAutoLocale";

@get external locale: t => option<bool> = "locale";

@get external get_name: t => string = "name";

@set external set_name: (t, string) => unit = "name";

@get external objectStore: t => IDBObjectStore.t = "objectStore";

@get external keyPath: t => IDBObjectStoreKeyPath.t = "keyPath";

@get external multiEntry: t => bool = "multiEntry";

@get external unique: t => bool = "unique"; 

/* Methods */

module Count = {
  @send external withKey: (t, IDBKeyRange.t<'key>) => IDBRequest.t<float>
    = "count";
  @send external withoutKey: t => IDBRequest.t<float> = "count";
};

let count = (~keyRange=?, t): IDBRequest.t<float> => {
  switch (keyRange) {
  | None => Count.withoutKey(t);
  | Some(kr) => Count.withKey(t, kr);
  };
};

module Get = {
  @send external withKey: (t, IDBKeyRange.t<'key>) => IDBRequest.t<'result>
    = "get";
  @send external withoutKey: t => IDBRequest.t<'result> = "get";
};

let get = (~keyRange=?, t): IDBRequest.t<'result> => {
  switch (keyRange) {
  | None => Get.withoutKey(t);
  | Some(kr) => Get.withKey(t, kr);
  };
};

module GetKey = {
  @send external withKey: (t, IDBKeyRange.t<'key>) => IDBRequest.t<'key>
    = "getKey";
  @send external withoutKey: t => IDBRequest.t<'key> = "getKey";
};

let getKey = (~key=?, t): IDBRequest.t<'key> => {
  switch (key) {
  | None => GetKey.withoutKey(t);
  | Some(k) => GetKey.withKey(t, k);
  };
};

module GetAll = {
  @send external without: t => IDBRequest.t<Js.Array.t<'result>> = "getAll";
  @send external withCount: (t, float) => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
  @send external withQuery: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
  @send external withQueryAndCount: (t, IDBKeyRange.t<'key>, float)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
};

let getAll = (~query=?, ~count=?, t: t): IDBRequest.t<Js.Array.t<'result>> => {
  switch (query) {
  | None => switch (count) {
      | None => GetAll.without(t);
      | Some(c) => GetAll.withCount(t, c);
    };
  | Some(q) => switch (count) {
      | None => GetAll.withQuery(t, q);
      | Some(c) => GetAll.withQueryAndCount(t, q, c);
    };
  };
};

module GetAllKeys = {
  @send external without: t => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external withCount: (t, float) => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external withQuery: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external withQueryAndCount: (t, IDBKeyRange.t<'key>, float)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
};

let getAllKeys = (~query=?, ~count=?, t: t): IDBRequest.t<Js.Array.t<'result>>
  => {
  switch (query) {
  | None => switch (count) {
      | None => GetAllKeys.without(t);
      | Some(c) => GetAllKeys.withCount(t, c);
    };
  | Some(q) => switch (count) {
      | None => GetAllKeys.withQuery(t, q);
      | Some(c) => GetAllKeys.withQueryAndCount(t, q, c);
    };
  };
};

module OpenCursor = {
  @send external without: t => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withRange: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withDirection: (t, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withRangeAndDirection:
    (t, IDBKeyRange.t<'key>, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
};

let openCursor = (~range=?, ~direction=?, t: t):
  IDBRequest.t<Types.iDbCursor<'value>>
  => {
  switch (range) {
  | None => switch (direction) {
      | None => OpenCursor.without(t);
      | Some(d) => OpenCursor.withDirection(t, d);
    };
  | Some(r) => switch (direction) {
      | None => OpenCursor.withRange(t, r);
      | Some(d) => OpenCursor.withRangeAndDirection(t, r, d);
    };
  };
};

module OpenKeyCursor = {
  @send external without: t => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withRange: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withDirection: (t, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withRangeAndDirection:
    (t, IDBKeyRange.t<'key>, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
};

let openKeyCursor = (~range=?, ~direction=?, t: t):
  IDBRequest.t<Types.iDbCursor<'value>>
  => {
  switch (range) {
  | None => switch (direction) {
      | None => OpenCursor.without(t);
      | Some(d) => OpenCursor.withDirection(t, d);
    };
  | Some(r) => switch (direction) {
      | None => OpenCursor.withRange(t, r);
      | Some(d) => OpenCursor.withRangeAndDirection(t, r, d);
    };
  };
};

