/* This binding makes no distinction between `IDBCursor` and 
 * `IDBCursorWithValue`. The `value` property is defined as an `option`. Use
 * `unit` for IDBCursor (without value). */

type t<'value> = Types.iDbCursor<'value>;

module Private = {
  type any;
  @get external source: t<'value> => any = "source";
  type typeName = [ #IDBObjectStore | #IDBIndex ];
  let getType: any => typeName = %raw(`x => x instanceof IDBIndex
                                      ? "IDBIndex"
                                      : "IDBObjectStore"
                                      `);
  let classify = (v: any): IDBCursorSource.t => 
    switch(v -> getType) {
    | #IDBObjectStore => IDBCursorSource.IDBObjectStore(v -> Obj.magic);
    | #IDBIndex => IDBCursorSource.IDBIndex(v -> Obj.magic);
    };

};

/* Properties */

let source = (t: t<'value>): IDBCursorSource.t =>
  t -> Private.source -> Private.classify;

@get external direction: t<'value> => IDBCursorDirection.t = "direction";

@get external key: t<'value> => 'key = "key";

@get external primaryKey: t<'value> => 'pk = "primaryKey";

@get external request: t<'value> => IDBRequest.t<'result> = "request";

@get external value: t<'value> => option<'value> = "value";

/* Methods */

@send external advance: (t<'value>, float) => unit = "advance";

module Continue = {
  @send external withKey: (t<'value>, 'key) => unit = "continue";
  @send external withoutKey: t<'value> => unit = "continue";
};

let continue = (~key=?, t: t<'value>): unit => {
  switch (key) {
  | None => Continue.withoutKey(t);
  | Some(k) => Continue.withKey(t, k);
  };
};

@send external continuePrimaryKey: (t<'value>, 'key, 'primaryKey) => unit
  = "continuePrimaryKey";

@send external delete: t<'value> => IDBRequest.t<unit> = "delete";

@send external update: (t<'value>, 'val) => IDBRequest.t<'key> = "update";

