type t<'result>;

/* properties */
@get external error: t<'result> => exn = "error";

@get external result: t<'result> => 'result = "result";

/* TODO source--not quite sure how to type it. MDN lists some possibilities, 
 * but it sounds nonexhaustive. */

@get
external readyState: t<'result> => IDBRequestReadyState.t = "readyState";

@get
external transaction: t<'result> => Js.Nullable.t<Types.iDBTransaction>
  = "transaction";

/* event handlers */

type errorHandler = Dom.errorEvent => unit;

@get external
get_onerror: t<'result> => Js.Nullable.t<errorHandler> = "onerror";

@set
external set_onerror: (t<'result>, errorHandler) => unit = "onerror";

type successHandler = Types.requestSuccessEvent => unit;

@get
external get_onsuccess: t<'result> => Js.Nullable.t<successHandler>
  = "onsuccess";

@set external
set_onsuccess: (t<'result>, successHandler) => unit = "onsuccess";
