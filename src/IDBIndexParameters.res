type t = {
  unique: bool,
  multiEntry: bool,
  locale: option<string>
};
