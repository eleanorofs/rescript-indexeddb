type t = { keyPath: IDBObjectStoreKeyPath.t, autoIncrement: bool };

type jsShared = {
  keyPath: IDBObjectStoreKeyPath.jsShared,
  autoIncrement: bool
};

let toJsShared = (t: t): jsShared => {
  keyPath: IDBObjectStoreKeyPath.toJsShared(t.keyPath),
  autoIncrement: t.autoIncrement
};
