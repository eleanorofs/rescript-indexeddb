type t;

module Private = {
  type any;
  @get external keyPath: t => any = "keyPath";
  type typeName = [ #null | #"object" | #"string" ];
  let getType: any => typeName = %raw(`x => x === null ? "null" : typeof x`);
  let classify = (v: any): IDBObjectStoreKeyPath.t => 
    switch(v -> getType) {
    | #null => IDBObjectStoreKeyPath.Null;
    | #"object" => IDBObjectStoreKeyPath.Array(v -> Obj.magic);
    | #"string" => IDBObjectStoreKeyPath.String(v -> Obj.magic);
    };
};

/* properties */
@get external indexNames: t => Js.Array.t<string> = "indexNames";

let keyPath = (t: t): IDBObjectStoreKeyPath.t =>
  t -> Private.keyPath -> Private.classify;

@get external name: t => string = "name";

@get external transaction: t => Types.iDBTransaction = "transaction";

@get external autoIncrement: t => bool = "autoIncrement";

/* methods */

module Add = {
  @send external withoutKey: (t, 'value) => IDBRequest.t<unit> = "add";
  @send external withKey: (t, 'value, 'key) => IDBRequest.t<'key> = "add";
};

let add = (t: t, ~key=?, value: 'value): IDBRequest.t<'key> => {
  switch (key) {
  | None => Add.withoutKey(t, value);
  | Some(k) => Add.withKey(t, value, k);
  };
};

@send external clear: t => IDBRequest.t<unit> = "clear";

module Count = {
  @send external withoutQuery: t => IDBRequest.t<float> = "count";
  @send external
  withQuery: (t, IDBKeyRange.t<'a>) => IDBRequest.t<float>
    = "count";
};

let count = (~query=?, t: t): IDBRequest.t<float> => {
  switch (query) {
  | None => Count.withoutQuery(t);
  | Some(q) => Count.withQuery(t, q);
  };
};

module CreateIndex = {
  @send external withoutParameters:
  (t, string,
   @unwrap [#Null(unit) | #String(string) | #Array(Js.Array.t<string>)])
    => Types.iDbIndex
    = "createIndex";
  @send external withParameters:
    (t, string,
     @unwrap [#Null(unit) | #String(string) | #Array(Js.Array.t<string>)],
     IDBIndexParameters.t)
    => Types.iDbIndex
    = "createIndex";

};

let createIndex = (t: t, indexName: string, ~parameters=?, keyPath: IDBObjectStoreKeyPath.t) => {
  switch (keyPath) {
  | Null => switch (parameters) {
      | None => CreateIndex.withoutParameters(t, indexName, #Null(()));
      | Some (ps) => CreateIndex.withParameters(t, indexName, #Null(()), ps);
    };
  | String(kp) => switch (parameters) {
      | None => CreateIndex.withoutParameters(t, indexName, #String(kp));
      | Some (ps) => CreateIndex.withParameters(t, indexName, #String(kp), ps);
    };
  | Array(kp) => switch (parameters) {
      | None => CreateIndex.withoutParameters(t, indexName, #Array(kp));
      | Some (ps) => CreateIndex.withParameters(t, indexName, #Array(kp), ps);
    };
  };
};

@send external delete: (t, IDBKeyRange.t<'key>) => IDBRequest.t<unit>
  = "delete";

@send external deleteIndex: (t, string) => unit = "deleteIndex";

@send external
get: (t, IDBKeyRange.t<'key>) => IDBRequest.t<'result> = "get";

@send external
getKey: (t, IDBKeyRange.t<'key>) => IDBRequest.t<'key>
  = "getKey";

module GetAll = {
  @send external
  without: t => IDBRequest.t<Js.Array.t<'result>> = "getAll";
  @send external
  withCount: (t, float) => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
  @send external
  withQuery: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
  @send external withQueryAndCount: (t, IDBKeyRange.t<'key>, float)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAll";
};

let getAll = (~query=?, ~count=?, t: t):
    IDBRequest.t<Js.Array.t<'result>> => {
  switch (query) {
  | None => switch (count) {
      | None => GetAll.without(t);
      | Some(c) => GetAll.withCount(t, c);
    };
  | Some(q) => switch (count) {
      | None => GetAll.withQuery(t, q);
      | Some(c) => GetAll.withQueryAndCount(t, q, c);
    };
  };
};

module GetAllKeys = {
  @send external without: t => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external
  withCount: (t, float) => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external
  withQuery: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
  @send external withQueryAndCount: (t, IDBKeyRange.t<'key>, float)
    => IDBRequest.t<Js.Array.t<'result>>
    = "getAllKeys";
};

let getAllKeys = (~query=?, ~count=?, t: t):
    IDBRequest.t<Js.Array.t<'result>>
    => {
      switch (query) {
          | None => switch (count) {
              | None => GetAllKeys.without(t);
              | Some(c) => GetAllKeys.withCount(t, c);
          };
          | Some(q) => switch (count) {
              | None => GetAllKeys.withQuery(t, q);
              | Some(c) => GetAllKeys.withQueryAndCount(t, q, c);
          };
      };
    };

@send external index: (t, string) => Types.iDbIndex = "index";

module OpenCursor = {
  @send external without: t => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withRange: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withDirection: (t, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
  @send external withRangeAndDirection:
    (t, IDBKeyRange.t<'key>, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<'value>>
    = "openCursor";
};

let openCursor = (~range=?, ~direction=?, t: t):
  IDBRequest.t<Types.iDbCursor<'value>>
  => {
  switch (range) {
  | None => switch (direction) {
      | None => OpenCursor.without(t);
      | Some(d) => OpenCursor.withDirection(t, d);
    };
  | Some(r) => switch (direction) {
      | None => OpenCursor.withRange(t, r);
      | Some(d) => OpenCursor.withRangeAndDirection(t, r, d);
    };
  };
};

module OpenKeyCursor = {
  @send external without: t => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withRange: (t, IDBKeyRange.t<'key>)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withDirection: (t, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
  @send external withRangeAndDirection:
    (t, IDBKeyRange.t<'key>, IDBCursorDirection.t)
    => IDBRequest.t<Types.iDbCursor<unit>>
    = "openKeyCursor";
};

let openKeyCursor = (~range=?, ~direction=?, t: t):
  IDBRequest.t<Types.iDbCursor<'value>>
  => {
  switch (range) {
  | None => switch (direction) {
      | None => OpenCursor.without(t);
      | Some(d) => OpenCursor.withDirection(t, d);
    };
  | Some(r) => switch (direction) {
      | None => OpenCursor.withRange(t, r);
      | Some(d) => OpenCursor.withRangeAndDirection(t, r, d);
    };
  };
};

module Put = {
  @send external withKey: (t, 'item, 'key) => IDBRequest.t<'key> = "put";
  @send external withoutKey: (t, 'item) => IDBRequest.t<'key> = "put";
};

let put = (t: t, ~key=?, item: 'item): IDBRequest.t<'key> => {
  switch (key) {
  | None => Put.withoutKey(t, item);
  | Some(k) => Put.withKey(t, item, k);
  };
};

