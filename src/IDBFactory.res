type t;

module Open = {
  @send external withoutVersion: (t, string)
    => IDBRequest.t<IDBDatabase.t>
    = "open";
  @send external withVersion: (t, string, float)
    => IDBRequest.t<IDBDatabase.t>
    = "open";
};

let open_ = (t: t, ~version=?, name: string) => {
  switch version {
  | None => Open.withoutVersion(t, name);
  | Some(v) => Open.withVersion(t, name, v);
  };
};

@send external deleteDatabase: (t, string) => IDBRequest.t<IDBDatabase.t>
  = "deleteDatabase";

@send external cmp: ('key, 'key) => int = "cmp";

@send external databases: t => Js.Array.t<DatabaseRecord.t> = "databases";
