include IDBRequest;

type eventHandler = Dom.event => unit;

@get external get_onblocked: t<IDBDatabase.t> => eventHandler = "onblocked";

@set external set_onblocked: (t<IDBDatabase.t>, eventHandler) => unit
  = "onblocked";

type upgradeHandler = IDBVersionChangeEvent.t => unit;

@get external get_onupgradeneeded: t<IDBDatabase.t> => upgradeHandler
  = "onupgradeneeded";

@set external set_onupgradeneeded:(t<IDBDatabase.t>, upgradeHandler)
  => unit
  = "onupgradeneeded";
