/* TypeScript, being what it is, REALLY takes an unopinionated approach and 
   uses the any type for upper and lower bounds. I see the value in that, but 
   for now I'm going to assume upper and lower bounds should be the same type. 
   We'll see if I live to regret it. 

   If you think my opinionated approach is bad and you want me to write a more
   literal binding for this package, open an issue and we'll have a discussion.
   I'm not immovable on this. 
*/

type t<'a>;

/* Properties */

@get external lower: t<'a> => option<'a> = "lower";

@get external lowerOpen: t<'a> => bool = "lowerOpen";

@get external upper: t<'a> => option<'a> = "upper";

@get external upperOpen: t<'a> => bool = "upperOpen";

/* Instance Methods */
@send external includes: (t<'a>, 'a) => bool = "includes";

/* Static Methods */

module Bound = {
  @scope("IDBKeyRange") @val
    external withFlags: ('a, 'a, bool, bool) => t<'a> = "bound";

  @scope("IDBKeyRange") @val
    external withoutFlags: ('a, 'a) => t<'a> = "bound";
};

module LowerBound = {
  @scope("IDBKeyRange") @val
    external withFlag: ('a, bool) => t<'a> = "lowerBound";

  @scope("IDBKeyRange") @val
    external withoutFlag: 'a => t<'a> = "lowerBound";
};

let lowerBound = (~open_=?, a:'a): t<'a> => {
  switch (open_) {
  | None => LowerBound.withoutFlag(a);
  | Some(o) => LowerBound.withFlag(a, o);
  };
};

@scope("IDBKeyRange") @val
  external only: 'a => t<'a> = "only";

module UpperBound = {
  @scope("IDBKeyRange") @val
    external withFlag: ('a, bool) => t<'a> = "upperBound";

  @scope("IDBKeyRange") @val
    external withoutFlag: ('a) => t<'a> = "upperBound";
};

let upperBound = (~open_=?, a:'a): t<'a> => {
  switch (open_) {
  | None => UpperBound.withoutFlag(a);
  | Some(o) => UpperBound.withFlag(a, o);
  };
};
