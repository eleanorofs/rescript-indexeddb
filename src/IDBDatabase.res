type t = Types.iDbDatabase;

module Private = {
  @send external
  createWithOptions: (t, string, CreateObjectStoreParameters.jsShared)
    => IDBObjectStore.t
    = "createObjectStore";
}

/* Properties */

@get external name: t => string = "name";

@get external version: t => float = "version";

@get external objectStoreNames: t => Js.Array.t<string> = "objectStoreNames";

/* Methods */

@send external close: t => unit = "close";

module CreateObjectStore = {
  @send external withoutOptions: (t, string) => IDBObjectStore.t
    = "createObjectStore";
  let withOptions = (t: t,
                     name: string,
                     options: CreateObjectStoreParameters.t): IDBObjectStore.t
    => options
    -> CreateObjectStoreParameters.toJsShared
    |> Private.createWithOptions(t, name);
};

let createObjectStore = (t: t, ~options=?, name: string): IDBObjectStore.t => {
  switch (options) {
  | None => CreateObjectStore.withoutOptions(t, name);
  | Some(o) => t ->
    CreateObjectStore.withOptions(name, o);
  };
};

@send external deleteObjectStore: (t, string) => unit = "deleteObjectStore";

module Transaction = {
  @send external withMode: (t, Js.Array.t<string>, IDBTransactionMode.t)
    => IDBTransaction.t
    = "transaction";
  @send external withoutMode: (t, Js.Array.t<string>)
    => IDBTransaction.t = "transaction";
};

let transaction = (t, ~mode=?, storeNames: Js.Array.t<string>):
  IDBTransaction.t => {
  switch (mode) {
  | None => Transaction.withoutMode(t, storeNames);
  | Some(m) => Transaction.withMode(t, storeNames, m);
  };
};

type handler = Dom.event => unit;

@get external get_onabort: t => handler = "onabort";

@set external set_onabort: (t, handler) => unit = "onabort";

@get external get_onclose: t => handler = "onclose";

@set external set_onclose: (t, handler) => unit = "onclose";

@get external get_onerror: t => handler = "onerror";

@set external set_onerror: (t, handler) => unit = "onerror";

type vcHandler = Dom.event => unit;

@get external get_onversionchange: t => vcHandler = "onversionchange";

@set external set_onversionchange: (t, vcHandler) => unit = "onversionchange";
