/* an implementation detail that breaks circular dependencies amongst modules.
 * Do not use publicly */

type iDbDatabase;

type iDbCursor<'value>;

type iDbIndex;

type iDBTransaction;

type requestSuccessEvent;



