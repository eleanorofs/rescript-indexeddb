type t = Types.requestSuccessEvent; 

@get external target: t => IDBRequest.t<'t> = "target";
