//TODO didn't bother binding the constructor because no one will ever use it.
//Open an issue if I'm wrong about that.

//TODO decide how I want to "extend event".
type t;

@get external oldVersion: t => float = "oldVersion";

@get external newVersion: t => float = "oldVersion";

/* inherited from event */
@get external target: t => IDBRequest.t<IDBDatabase.t> = "target";
